Pasos para provisionamiento con ansible de vm devel y prod
----------------------------------------------------------
- Playbook's existentes:
	playbook.yml
		Provisiona de todos los roles a la vez al grupo de hosts indicado
	playbook_common.yml
		Provisiona del rol common al grupo de hosts indicado
	playbook_php.yml
		Provisiona del rol php al grupo de hosts indicado
	playbook_composer.yml
		Provisiona del rol composer al grupo de hosts indicado

- llamada a playbook's. Los siguientes playbook disponen de un parametro para indicar el grupo de hosts al que se quiere provisionar. A continuacón se especifica su uso:
	parametro:
		Nombre: entorno
		valor por defecto: devel

	valores aceptados:
		devel
		prod
		centos

	playbook que lo aceptan:
		playbook.yml
		playbook_common.yml
		playbook_php.yml
		playbook_composer.yml

	ejemplo de uso:
		ansible-playbook <fichero yml a utilizar> -i <fichero hosts a utilizar> -e entorno=<nombre de entorno a provisionar>